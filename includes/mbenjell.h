/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mbenjell.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 17:54:17 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 16:41:46 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MBENJELL_H
# define MBENJELL_H

# include "libft.h"
# include "fcntl.h"

int		ft_check_quote(char *str);
int		strlen_mbenjell(char *str);
char	**parse_mbenjell(char *str, int words);

void	controller(char *name, char *file);

void	print_tab_error(int line, int tab, int tabbed);
void	print_func_error(int line, int error, int val);
void	print_small_error(int line, int error);
void	print_inter_error(int line, char *error);

void	check_tabulations(char **tab);
void	check_makefile(char **tab);
void	check_header(char **tab);
void	check_function(char **tab);
void	check_comments(char **tab);

#endif
