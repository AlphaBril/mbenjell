# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/04 18:02:25 by edjubert          #+#    #+#              #
#    Updated: 2019/05/24 19:38:25 by fldoucet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			:=		mbenjell

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  DIRECTORIES                                 #

SRC_DIR			:=		./srcs
INC_DIR			:=		./includes
INC_LIB			:=		./libft/includes
OBJ_DIR			:=		./obj
LIBFT_FOLDER	:=		./libft
LIBFT			:=		$(LIBFT_FOLDER)/libft.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     FILES                                    #

SRC				:=		mbenjell.c			\
						utils_mbenjell.c	\
						print_errors.c		\
						controller.c		\
						check_comments.c	\
						check_formatting.c	\
						check_function.c	\
						check_headers.c		\
						check_makefile.c	\
						check_prefix.c		\
						check_tabulations.c

OBJ				:=		$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))
NB				:=		$(words $(SRC))
INDEX			:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                               COMPILER & FLAGS                               #

GCC				:=		gcc
FLAGS			:=		-Wall -Wextra -Werror -g

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     RULES                                    #

all:				$(NAME)

$(NAME):			$(OBJ)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) --no-print-directory; fi
	@ $(GCC) $(FLAGS) $(OBJ) $(LIBFT) -o $(NAME)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "Compilation of " $(NAME) " is done ---"

$(OBJ_DIR)/%.o:		$(SRC_DIR)/%.c
	@ mkdir -p $(OBJ_DIR)
	@ $(eval DONE = $(shell echo $$(($(INDEX) * 20 / $(NB)))))
	@ $(eval PERCENT = $(shell echo $$(($(INDEX) * 100 / $(NB)))))
	@ $(eval TO_DO = $(shell echo "$@"))
	@ $(GCC) $(FLAGS) -I$(INC_LIB) -I$(INC_DIR) -c $< -o $@
	@ printf "\033[33m[ %3d%% ] %s\t%s\r\033[0m" $(PERCENT) $(NAME) $@
	@ $(eval INDEX = $(shell echo $$(($(INDEX) + 1))))

clean:
	@ /bin/rm -rf $(OBJ_DIR)
	@ make -C $(LIBFT_FOLDER) clean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "CLEAN  of " $(NAME) " is done ---"

fclean:				clean
	@ /bin/rm -rf $(NAME)
	@ make -C $(LIBFT_FOLDER) fclean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "FCLEAN of " $(NAME) " is done ---"

re:				fclean all

.PHONY: all clean fclean re
