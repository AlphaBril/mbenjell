/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_headers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 13:06:28 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 10:12:41 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

/*
** Seuls les inclusions de headersm les declarationsm les definesm les
** prototypes et les macros sont autorises dans les fichiers headers.
** Tous les includes de .h doivent se faire au debut du fichier
** On protegera les headers contre la double inclusionm si le fichier
** est ft_foo.hm la macro temoin est FT_FOO_H
** Une inclusion de header dont on ne se sert pas est interdite (bon courage
** pour checker ca mec)
** Les define aue vous creew ne doivent etre utilises que pour associer des
** valeurs litterales et constantesm et rien d'autre (check alphanumerique
** sans espaces sinon warning correcteur)
** Seuls les noms de macros sont en majuscules
** Il faut indenter les les caracteres qui suivent un #if, #ifdef ou #ifndef
*/

void		check_header(char **tab)
{
	int		i;

	i = 0;
	while (tab[i])
		i++;
}
