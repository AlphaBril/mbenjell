/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_function.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 12:51:33 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 12:33:41 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

/*
** Chaque fonction doit faire au maximum 25 lignes sans compter les accolades
** du bloc de la fonction. DONE
** Chaque ligne ne peut faire plus de 80 colonnes commentaires compris. DONE
** Une fonction prend au maximum 4 parametres nommes DONE
** Une fonction qui ne prend pas d'argument doit explicitement etre prototypee
** avec le mot void comme argument. DONE
** Les parametres des prototypes de fonctions doivent etre nommes wtf ?
** chaque definition de fonction doit etre separee par une ligne vide de
** la suivante. DONE
** Vous ne pouvew declarer que 5 variables par bloc au maximum DONE
** Vos identifiants de fonctions doivent etre alignes dans un meme fichier DONE
** Les prototypes de fonctions doivent se trouver uniquement dans les .h DONE
** Interdiction de for, do, switch, case, goto, DONE
** Vous ne pouvez pas include de .c DONE
** Vous ne pouvew pas avoir plus de 5 definitions de fonctions dans un .c DONE
*/

int		check_ft(char *str, int line)
{
	int		i;
	int		param;

	i = 0;
	param = 1;
	if (str[0] != '\t' && str[0] != '{' && str[0] != '}' && str[0] != '*'
		&& str[0] != '\n' && str[0] != '#' && str[0] != '/' && str[0] != '\0')
	{
		while (str[i])
		{
			if (str[i] == '(' && str[i + 1] == ')')
				ft_printf("   -Function on line {magenta}[%d]{eoc} \
					need to have void as argument", line);
			if (str[i] == ',')
				param++;
			if (str[i] == ';')
				print_func_error(line, 3, 0);
			i++;
		}
		if (param > 4)
			print_func_error(line, 2, param);
		return (1);
	}
	return (0);
}

void	check_function_length(char **tab, int *sample)
{
	int		i;
	int		j;

	*sample = 0;
	i = 0;
	while (tab[i])
	{
		if (tab[i][0] == '{')
		{
			j = i + 1;
			while (tab[j][0] != '}')
			{
				if (tab[j][0] == '\n' && (j - (i + 1)) > 5)
				{
					ft_printf("  -Function on line {magenta}[%d]{eoc} ", i);
					ft_printf("have {red}[%d]{eoc} variables,", (j - (i + 1)));
					ft_printf(" can be up to {green}[%s]{eoc}\n", "5");
				}
				j++;
			}
			if ((j - (i + 1)) > 25)
				print_func_error(i, 5, j - (i + 1));
		}
		i++;
	}
}

void	check_inside(char *str, int line)
{
	int		i;

	i = 0;
	while (str[i])
	{
		i = i + ft_check_quote(&str[i]);
		if (!ft_strncmp(&str[i], "for ", 4) || !ft_strncmp(&str[i], "for(", 4))
			print_inter_error(line + 1, "for");
		if (!ft_strncmp(&str[i], "do\n", 3) || !ft_strncmp(&str[i], "do{", 3))
			print_inter_error(line + 1, "do");
		if (!ft_strncmp(&str[i], "switch ", 7)
			|| !ft_strncmp(&str[i], "switch(", 7))
			print_inter_error(line + 1, "switch");
		if (!ft_strncmp(&str[i], "case ", 5))
			print_inter_error(line + 1, "case");
		if (!ft_strncmp(&str[i], "goto ", 5))
			print_inter_error(line + 1, "goto");
		i++;
	}
}

void	check_global_scope(char *str, int line, int *sample)
{
	int		i;
	int		calc;

	i = 0;
	while (str[i])
	{
		if (str[i] == '\t')
		{
			calc = i;
			while (str[i++] == '\t')
				calc = calc + (4 - (calc % 4));
			if (*sample == 0)
				*sample = calc;
			else if (calc != *sample)
			{
				ft_printf("  -Global scope {red}%s{eoc} aligned ", "bad");
				ft_printf("(check line {magenta}[%d]{eoc})\n", line);
			}
		}
		i++;
	}
}

void	check_function(char **tab)
{
	int		i;
	int		ft;
	int		sample;

	i = 12;
	ft = 0;
	check_function_length(tab, &sample);
	while (tab[i])
	{
		if (check_ft(tab[i], i + 1) == 1)
			check_global_scope(tab[i], i + 1, &sample);
		if (tab[i][0] == '#' && tab[i][ft_strlen(tab[i]) - 3] == 'c')
			print_func_error(i + 1, 8, 0);
		if (tab[i][0] == '\n' && tab[i + 1][0] == '\n')
			print_func_error(i + 1, 6, 0);
		if (ft_strlen(tab[i]) > 2 && tab[i][ft_strlen(tab[i]) - 2] == ' ')
			print_func_error(i + 1, 7, 0);
		if (strlen_mbenjell(tab[i]) > 80)
			print_func_error(i + 1, 0, strlen_mbenjell(tab[i]));
		ft = ft + check_ft(tab[i], i + 1);
		check_inside(tab[i], i);
		i++;
	}
	if (ft > 5)
		print_func_error(0, 1, ft);
}
