/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_makefile.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 13:30:13 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 10:12:57 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

/*
** Les regles $(NAME)m cleam fcleanm re et all sont obligatoires
** Le projet est considere comme non fonctionnel si le Makefile relink
** Dans le cas d'un projet multibinairem en plus des regles precedentes
** vous devrez avoirune regle all compilant les deux binaires ainsi qu'une
** regle specifique a chaque binaire compile
** Les sources necessaires a la compilation de votre programme doivent etre
** explicitement cites dans votre makefile (pas de wildcard)
*/

void		check_makefile(char **tab)
{
	int		i;

	i = 0;
	while (tab[i])
		i++;
}
