/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_mbenjell.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 17:36:41 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/19 15:00:23 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

int		ft_check_quote(char *str)
{
	int i;

	i = 0;
	if (str[i] == '\"')
	{
		i++;
		while (str[i] && str[i] != '\"')
			i++;
	}
	if (str[i] == '\'')
	{
		if (str[i + 1] == '\\')
			i++;
		i = i + 3;
	}
	return (i);
}

int		strlen_mbenjell(char *str)
{
	int		i;
	int		ret;

	i = 0;
	ret = 0;
	while (str[i])
	{
		if (str[i] == '\t')
			ret = ret + (4 - (ret % 4));
		else
			ret++;
		i++;
	}
	return (ret);
}

char	**parse_mbenjell(char *str, int words)
{
	int		i;
	int		j;
	int		start;
	char	**ret;

	i = -1;
	while (str[++i])
		if (str[i] == '\n')
			words++;
	words++;
	if (!(ret = (char**)malloc(sizeof(char*) * (words + 1))))
		return (NULL);
	i = 0;
	j = 0;
	while (i < words)
	{
		start = j;
		while (str[j] && str[j] != '\n')
			j++;
		ret[i] = ft_strsub(str, start, (++j) - start);
		i++;
	}
	ret[i] = NULL;
	return (ret);
}
