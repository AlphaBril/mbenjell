/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mbenjell.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 17:51:56 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/24 19:00:52 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

int		ft_swallow_mbenjell(int fd, char **line)
{
	int		rd;
	char	buf[4096 + 1];

	rd = 1;
	while (rd != 0)
	{
		if ((rd = read(fd, buf, 4096)) == -1)
			return (-1);
		buf[rd] = '\0';
		if (!(*line = ft_strjoin_free(*line, buf, 1)))
			return (-1);
	}
	return (0);
}

void	print_error(int error)
{
	if (error == 0)
		ft_printf("   Stop being a {underlined}dumbass{eoc} %C\n", L'😡');
	if (error == 1)
	{
		ft_printf("usage : ./mbenjell Makefile\n");
		ft_printf("\t./mbenjell **/**.[ch] (zsh)\n");
	}
	if (error == 2)
		ft_printf("{red}%s{eoc}\n", "  Cannot open File.");
}

int		ft_open_and_check(char *str)
{
	int		fd;
	char	*file;

	file = NULL;
	fd = open(str, O_RDONLY);
	ft_printf("Norme : %s\n", str);
	if (!ft_strcmp(str, "/dev/random") || !ft_strcmp(str, "/dev/zero"))
		print_error(0);
	else if (ft_swallow_mbenjell(fd, &file) == -1)
		print_error(2);
	else
		controller(str, file);
	ft_strdel(&file);
	return (0);
}

int		main(int ac, char **av)
{
	int		i;

	if (ac <= 1)
		print_error(1);
	i = 1;
	while (av[i])
	{
		ft_open_and_check(av[i]);
		i++;
	}
	return (0);
}
