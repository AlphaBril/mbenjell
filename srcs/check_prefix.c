/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_prefix.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 12:25:37 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/24 19:53:49 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

/*
** Un nom de structure doit commencer par s_
** Un nom de typedef doit commencer par t_
** Un nom d'union doit commencer par u_
** Un nom d'enum doit commencer par e_
** Un nom de globale doit commencer par g_
** Les noms de variables, de fonctions, de fichiers et de repertoires doivent
** etre compose exclusivement de minusculesm de chiffres et de '_'
** Le fichier doit etre compatible
** Les caracteres ne faisant pas partie de la table ascii ne sont pas autorises
*/

void	check_prefix(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
}
