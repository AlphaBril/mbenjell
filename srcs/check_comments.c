/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_comments.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 13:23:38 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 16:53:53 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

/*
** Il ne doit pas y avoir de commentaires dans le corps d'une fonction
** Les commentaires sont commences et termines par une ligne seulem toute les
** lignes intermediaires s'alignent sur ellesm et commences par "**"
** Les commentaires commencant par // sont interdits
*/

void		check_well_formatted_comments(char **tab)
{
	int		i;
	int		check;

	i = 12;
	check = 0;
	while (tab[i])
	{
		if (tab[i][0] && tab[i][0] == '{')
			check = 1;
		if (tab[i][0] && tab[i][0] == '}')
			check = 0;
		if (tab[i][0] && tab[i][1] && tab[i][0] == '/' && tab[i][1] == '*')
		{
			if (check == 1)
				print_small_error(i + 1, 2);
			i++;
			if (tab[i][0] != '*' || tab[i][1] != '*')
				print_small_error(i + 1, 1);
			while (tab[++i] && tab[i][0] && tab[i][1]
				&& tab[i][0] != '*' && tab[i][1] != '/')
				if (tab[i][0] != '*' || tab[i][1] != '*')
					print_small_error(i + 1, 1);
		}
		i++;
	}
}

void		check_forbidden_comments(char *str, int line)
{
	int		i;

	i = 0;
	while (str[i])
	{
		i = i + ft_check_quote(&str[i]);
		if (!ft_strncmp(&str[i], "//", 2) && str[0] != '*' && str[1] != '*')
			print_inter_error(line + 1, "C++ comments");
		i++;
	}
}

void		check_comments(char **tab)
{
	int		i;
	int		j;

	i = 0;
	check_well_formatted_comments(tab);
	while (tab[i])
	{
		check_forbidden_comments(tab[i], i);
		j = 0;
		while (tab[i][j])
		{
			j = j + ft_check_quote(&tab[i][j]);
			if (!ft_strncmp(&tab[i][j], "/*", 2) && i > 12 && j != 0)
				print_small_error(i + 1, 1);
			j++;
		}
		i++;
	}
}
