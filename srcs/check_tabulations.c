/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tabulations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 17:32:46 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/19 15:18:44 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

int		check_condi(char *str, int line, int start, int end)
{
	int		i;
	int		tab;
	int		tabbed;

	tab = 0;
	while (str[tab] && str[tab] == '\t')
		tab++;
	i = tab;
	while (str[i++] && str[i] != ';' && str[i] != '{' && !(tabbed = 0))
	{
		i = i + ft_check_quote(&str[i]);
		line = (str[i] == '\n') ? line + 1 : line;
		start = (str[i] == '(') ? (start + 1) : start;
		end = (str[i] == ')') ? (end + 1) : end;
		if (str[i] == '\n' && str[i + 1] == '\t' && start != end)
		{
			while (str[i++] && str[i] == '\t')
				tabbed++;
			i--;
			if (tabbed != (tab + start - end))
				print_tab_error(line, (tab + start - end), tabbed);
		}
		else if (str[i] == '\n' && str[i + 1] == '\t' && start == end)
			return (line);
	}
	return (line);
}

void	check_operators(char *str, int line)
{
	int		length;

	length = ft_strlen(str);
	if (length > 4 && (!ft_strncmp(&str[length - 4], " ||\n", 4)
			|| !ft_strncmp(&str[length - 4], " &&\n", 4)
			|| !ft_strncmp(&str[length - 3], " =\n", 3)
			|| !ft_strncmp(&str[length - 3], " +\n", 3)
			|| !ft_strncmp(&str[length - 3], " -\n", 3)
			|| !ft_strncmp(&str[length - 3], " *\n", 3)
			|| !ft_strncmp(&str[length - 3], " /\n", 3)
			|| !ft_strncmp(&str[length - 3], " %\n", 3)))
		print_small_error(line + 1, 0);
}

int		check_equation(char *str, int line, int start, int end)
{
	int		i;
	int		tab;
	int		tabbed;

	tab = 0;
	while (str[tab] && str[tab] == '\t')
		tab++;
	i = tab;
	while (str[++i] && str[i] != ';' && str[i] != '{')
	{
		tabbed = 0;
		i = i + ft_check_quote(&str[i]);
		line = (str[i] == '\n') ? line + 1 : line;
		start = (str[i] == '(') ? (start + 1) : start;
		end = (str[i] == ')') ? (end + 1) : end;
		if (str[i] == '\n' && str[i + 1] == '\t' && start != end)
		{
			while (str[i++] && str[i] == '\t')
				tabbed++;
			i--;
			if (tabbed != (tab + start - end) && !ft_match(str, "*if*")
				&& !ft_match(str, "*while*") && !ft_match(str, "*return*"))
				print_tab_error(line, (tab + start - end), tabbed);
		}
	}
	return (line);
}

void	check_pre_condi(char **tab, int *i)
{
	char	*to_check;
	int		z;

	to_check = NULL;
	if (ft_strlen(tab[*i]) > 2 && (ft_match(tab[*i], "* = *")
			&& tab[*i][ft_strlen(tab[*i]) - 2] != ';'))
	{
		z = *i;
		while (tab[z] && tab[z][ft_strlen(tab[z]) - 2] != ';')
			to_check = ft_strjoin_free(to_check, tab[z++], 1);
		to_check = ft_strjoin_free(to_check, tab[z], 1);
		*i = check_equation(to_check, *i + 1, 1, 0) - 1;
		ft_strdel(&to_check);
	}
	if ((ft_strlen(tab[*i]) > 2 && tab[*i][ft_strlen(tab[*i]) - 2] != ';')
		&& ft_nmatch(tab[*i], "*(*") != ft_nmatch(tab[*i], "*)*"))
	{
		z = *i;
		while (tab[z] && tab[z][ft_strlen(tab[z]) - 2] != '{'
			&& tab[z][ft_strlen(tab[z]) - 2] != ';')
			to_check = ft_strjoin_free(to_check, tab[z++], 1);
		to_check = ft_strjoin_free(to_check, tab[z], 1);
		*i = check_condi(to_check, *i + 1, 0, 0) - 1;
		ft_strdel(&to_check);
	}
}

void	check_tabulations(char **tab)
{
	int		i;

	i = 0;
	while (tab[i])
	{
		check_operators(tab[i], i);
		check_pre_condi(tab, &i);
		i++;
	}
}
