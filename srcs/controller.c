/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controller.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 14:02:57 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/19 14:12:48 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

void		launch_makefile_test(char **tab)
{
	check_makefile(tab);
}

void		launch_header_test(char **tab)
{
	check_header(tab);
}

void		launch_c_test(char **tab)
{
	check_function(tab);
	check_comments(tab);
	check_tabulations(tab);
}

void		controller(char *name, char *str)
{
	char	**tab;
	int		i;

	i = 0;
	tab = parse_mbenjell(str, 0);
	if (ft_match(name, "*.c"))
		launch_c_test(tab);
	else if (ft_match(name, "*.h"))
		launch_header_test(tab);
	else if (!ft_strcmp(name, "Makefile"))
		launch_makefile_test(tab);
	else
		ft_printf("{red}%s{eoc}\n", "   File not supported.");
	while (tab[i])
		ft_strdel(&tab[i++]);
	free(tab);
}
