/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_formatting.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 13:04:29 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 10:12:13 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

/*
** Vous devez indenter votre code avec des tabulations de la taille de 4 espace
** Une seule instruction par ligne
** Une ligne vide ne doit pas contenir d'espace ou de tabulation
** Quand vous rencontrer une accolade ouvrante ou fermante ou une fin de
** structure de controlem vous devew retourner a la ligne
** Chaque virgule ou point-virgule doit etre suivi d'un espace sauf en fin de
** ligne.
** Chaque operateur (binaire ou ternaire) et operandes doivent etre separes
** par un espace et seulement un
** Chaque mot clef du C doit etre suivis d'un espace, sauf pour ceux de type
** (int, char, float, etc) ainsi que sizeof
** Chaque declaration de variable doit etre indentee sur la meme colonne
** Les etoiles de pointeurs doivent etre colles au nom de la variable
** Une seule declarationd e variable par ligne
** On ne peut pas faire une declaration et une initialisation sur une meme
** ligne, a l'exception des variables globales et des variables statiques
** Les delcarations doivent etre en debut de fonction et doivent etre
** separees de l'implementation par une ligne vide
** Aucune ligne vide ne doit etre presente au milieu des declarations ou
** de l'implementation
** la multiple assignation est interdite
** tableau a taille variable : int tab[x] interdit
** Ternaires imbriques interdites
*/

void		check_formatting(char **tab)
{
	int		i;

	i = 0;
	while (tab[i])
		i++;
}
