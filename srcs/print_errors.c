/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_errors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 13:49:38 by fldoucet          #+#    #+#             */
/*   Updated: 2019/06/07 16:53:51 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mbenjell.h"

void		print_tab_error(int line, int tab, int tabbed)
{
	ft_printf("  -Wrong number of tabulations at ");
	ft_printf("line {magenta}[%d]{eoc} :\n", line);
	ft_printf("\tShould be {green}[%d]{eoc} but is ", tab);
	ft_printf("{red}[%d]{eoc}\n", tabbed);
}

void		print_small_error(int line, int error)
{
	if (error == 0 && line > 12)
		ft_printf("  -Operator at end of line {magenta}[%d]{eoc}\n", line);
	if (error == 1)
		ft_printf("  -Comment on line {magenta}[%d]{eoc}%s",
			line, " not well formatted\n");
	if (error == 2)
		ft_printf("  -Comment on line {magenta}[%d]{eoc}%s",
			line, " cannot be placed inside a function\n");
}

void		print_inter_error(int line, char *error)
{
	ft_printf("%s{red}[%s]{eoc} on line {magenta}[%d]{eoc}\n",
		"  -Use of forbidden ", error, line);
}

void		print_func_error(int line, int error, int val)
{
	if (error == 0)
		ft_printf("%s{magenta}[%d]{eoc}%s{red}[%d]{eoc}%s{green}[80]{eoc}\n",
			"  -Line ", line, " has ", val, " characters, can be up to ");
	if (error == 1)
		ft_printf("  -File contain {red}[%d]{eoc}%s{green}[5]{eoc}\n",
			val, " functions, can be up to ");
	if (error == 2)
		ft_printf("%s{magenta}[%d]{eoc} has {red}[%d]{eoc}%s{green}[4]{eoc}\n",
			"  -Function on line ", line, val, " parameters, can be up to ");
	if (error == 3)
		ft_printf("  -Function on line {magenta}[%d]{eoc}%s\n",
			line, " is a prototype, which is forbidden");
	if (error == 4)
		ft_printf("  -Function on line {magenta}[%d]{eoc}%s\n",
			line, " isn't well aligned");
	if (error == 5)
		ft_printf("%s{magenta}[%d]{eoc} %s {red}[%d]{eoc}%s{green}[25]{eoc}\n",
			"  -Function on line ", line, "have", val, " lines can be up to ");
	if (error == 6)
		ft_printf("  -Multiples empty lines after line {magenta}[%d]{eoc}\n",
			line);
	if (error == 7)
		ft_printf("  -Space at the end of line {magenta}[%d]{eoc}\n", line);
	if (error == 8)
		ft_printf("  -Include on line {magenta}[%d]{eoc} is a c_file\n", line);
}
